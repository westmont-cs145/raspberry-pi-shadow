Overview: 
ShadowPi is a interactive game that takes greenscreen images of the user and puts them into a video format similar to stop motion, transforms the image into a shadow image and uses it as the player avatar. 
The game consists of falling obsticles and the goal is to avoid the obstacles which, if collided into will trigger a game over. 
Goal: Dodge the obstacles falling from the sky, if colided into - Game Over :( -- The longer you're alive the more difficult it gets.  

Librarys: 
PIL, math, random, time, picamera, sys, threading, pygame, png

Instructions:
Download all required files: 
    obstacleManager.py
    displayController.py
    compareShadows.py
    pypngTest.py
    Obstacle.py
    png.py
from ShadowPi Bitchbucket repository. Connect a generic HD pi camera to a raspberryPi 4, sudo into raspi-config and 'enable' camera in the configuration, and clear a space large enough for mobility with a clear green background. 

Files: 
displayController.py - Actual game file that runs and compiles - followed by 3 int values(x y z)
x - speed of obstacle
y - size of the screen display (multiplication of the default (160x144) size)
z - sensitivity of the pixel collision check

Andrew Day: Display creator, pixel-level algorithms, configuration
Josh Cross: Collision Detection, obstacle creation, Claire's Git intermediary
Claire Inglehart: Display creator, code testing
