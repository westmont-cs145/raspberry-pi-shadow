# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 12:47:15 2021

@author: Owner
"""

import Obstacle
import random
from PIL import Image
#activeObstacles is a list of all Obstacles currently in use
def obstacleManager(activeObstacles = [], fallSpeed):
    #_activeObstacles is the copy of the list that will be worked with and returned
    _activeObstacles = activeObstacles
    
    #FALLPERUNIT is the number of pixles an obstacle will move down each time its position is updated
    FALLPERUNIT = fallSpeed

    
    #move all obstacles down
    reaper = False
    for obstacle in _activeObstacles:
        obstacle.pushDown(FALLPERUNIT)
        if(obstacle.getYoff() > 144-FALLPERUNIT):
            reaper = True
    if(reaper == True):
        _activeObstacles.pop(0)
        reaper = False
    
    #create obstacle if appropriate
    if(len(activeObstacles) < 4):
        xOff = random.randint(0, 130) #placeholder, this will respond to the player's location later
        img = Image.open("baseObs.png", "r")
        newObs = Obstacle.Obstacle(img, xOff, 0)
        _activeObstacles.append(newObs)
    
    #collision detection??? (Probably not but if we decide to include it in this it goes here)
    
    #return the new activeObstacles list
    return _activeObstacles