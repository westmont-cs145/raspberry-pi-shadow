# -*- coding: utf-8 -*-
"""
Created on Sat Apr 17 14:15:02 2021

@author: Owner
"""

import time
import picamera
class CameraLoop:
    def __init__(self):
        self.picName=1 #access by accessing ((objectName.picName)-1)
        self.running = True
        with picamera.PiCamera() as self.camera:

            self.camera.resolution = (1280, 720)
            self.camera.framerate = 30
            # Wait for the automatic gain control to settle
            time.sleep(2)
            # Now fix the values
            self.camera.shutter_speed = self.camera.exposure_speed
            self.camera.exposure_mode = 'off'
            g = self.camera.awb_gains
            self.camera.awb_mode = 'off'
            self.camera.awb_gains = g
            # Finally, take photo
        
        
            
    def loop(self):
        while(self.running):
            self.camera.capture('im'+self.picName+'.png')
            self.picName=(self.picName+1)%10
            time.sleep(1/20)
        ##busy Wait solution##
#        while(True):
#            while(self.running):
#                self.camera.capture('im'+self.picName+'.png')
#                self.picName=(self.picName+1)%60
#                time.sleep(1/20)
    
    def mostRecentPic(self):
        return ('im'+((self.picName-1)%80)+'.png')