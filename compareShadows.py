# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 10:33:46 2021
(Last modified 4/11/2021)

@author: Joshua Cross for CS-145 shadowpi Project

This code is intended to compare input silouette images and return true
if there is any overlap between the them.
"""
#simport pypngTest
#import copy
#import png
#from PIL import Image
#import time

"""
compareShadows takes in png images and compares them to see if they have
black pixles in the same places. returns true if there is any shadow overlap
between them
"""
def compareShadows(playerInput, obstacleOffSet):
    #startTime = time.perf_counter_ns()
    """i is the iterator variable and will pass through any neccessary pixles
    Josh's note for Josh: This may be modified by many different aspects, but
    should remain trustworthy
    """
    obstacleInputBase = [[5, 9], [3, 11], [2, 12], [1, 13], [1, 13], [0, 15], [0, 15], [0, 15], [0, 15], [0, 15], [1, 13], [1, 13], [2, 12], [3, 11], [5, 9]]
    
    for obstacles in obstacleOffSet:
        for ranges in obstacleInputBase:
            for p_range in playerInput[obstacles[1]]:
                if(p_range[0] != -1):
                    #if the start point is in the obstacle
                    if((ranges[0] + obstacles[0] >= p_range[0]) and (ranges[0] + obstacles[0] <= p_range[1])):
                        return True
                    #if the end point is in the obstacle
                    if((ranges[1] + obstacles[0] >= p_range[0]) and (ranges[1] + obstacles[0] <= p_range[1])):
                        return True
                    #if the start and end points are outside the obstacle
                    #if((ranges[0] + obstacles[0] <= p_range[0]) and (ranges[1] + obstacles[0] >= p_range[1])):
                    #    return True
                    #if the start an
                    if((ranges[0] + obstacles[0] >= p_range[0]) and (ranges[1] + obstacles[0] <= p_range[1])):
                        return True
    #if no collision is detected, return False
    return False
    #endTime = time.perf_counter_ns()
    #print("This took " + (endTime - startTime) + " ns")

    
    
    
    
    
    
    
    
    
    
    """
    print("placeholder while I get used to python again");
    
    #pWidth is the width of the player image
    pWidth = None
    #pHeight is the height of the player image
    pHeight = None
    #oWidth is the width of the player image
    oWidth = None
    #oHeight is the height of the player image
    oHeight = None
    
    #opens the playerInput image
    with open(playerInput, 'rb') as player:
        #creates a Reader object to process the player image
        playerReader = png.Reader(player)
        #playerData holds all of the data of the player image in a list
        playerData = playerReader.read()
        #pWidth is the width of the player image
        pWidth = playerData[0]
        #pHeight is the height of the player image
        pHeight = playerData[1]
        #opens the playerInput image
        with open(obstacleInput, 'rb') as obstacle:
            #creates a Reader object to process the obstacle image
            obstacleReader = png.Reader(obstacle)
            #obstacleData holds all of the data of the obstacle image in a list
            obstacleData = obstacleReader.read()
            #oWidth is the width of the player image
            oWidth = obstacleData[0]
            #oHeight is the height of the player image
            oHeight = obstacleData[1]
    
    #gets the pixles of the player image as a list and puts it into pPixles
    playerImg = Image.open(playerInput)
    pPixles = list(playerImg.getdata())
    
    
    
    #gets the pixles of the obstacle image as a list and puts it into oPixles
    obstacleImg = Image.open(obstacleInput)
    oPixles = list(obstacleImg.getdata())

    #step and counter variables only here for debug purposes
    step = 1
    
    counter = 0
    
    #the next few lines can be removed/modified if we decided to ensure that
    #the images are the same size by this point
    smaller = None
    
    pSize = pWidth * pHeight
    oSize = oWidth * oHeight
    
    if (pSize) > (oSize):
        smaller = oSize
    else:
        smaller = pSize
        
    for checkIn in range(0, smaller, step):
        print(pPixles[checkIn])
        print(oPixles[checkIn])
        
        ""\"if it becomes clear that there are more opaque pixles in oPixles, 
        switch these two""\"
        if pPixles[checkIn] [len(pPixles[checkIn])-1] == 255 and oPixles[checkIn] [len(pPixles[checkIn])-1] == 255:
            print("collison detected at (" + str(checkIn%pWidth) + ", " + str(checkIn%pHeight) +").")
            print("counter is " + str(counter))
            return True
        
        counter = counter + 1
    
        
compareShadows("output.png", "joshOutput.png")
"""

#print(compareShadows(pypngTest.pypngTest('TestingWall.png', 'test-out.png', 5),[[60,10],[75,1],[36,29],[120,70]]