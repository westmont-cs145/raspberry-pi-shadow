# -- coding: utf-8 --
"""
Created on Fri Apr 23 14:02:26 2021

@author: Joshua Cross
"""

import obstacleManager
import imageCompilation
from PIL import Image
import pygame
import time

obstacles = obstacleManager.obstacleManager()
i = 10

running = True
while running:


    while(i != 0):
        fusion = Image.open("shadowtest3.png", "r")

        obstacleManager.obstacleManager(obstacles)
        for img in obstacles:
            fusion = imageCompilation.imageCompilation(fusion, img.getBaseImg())

        fusion.save("testing1234.png")

        fusion = pygame.image.load("testing1234.png")
        back = pygame.image.load("yellow.png")

        screen = pygame.display.set_mode(fusion.get_size())

        screen.blit(back, (0,0))
        pygame.display.flip()

        screen.blit(fusion, (0,0))
        pygame.display.flip()
        time.sleep(0.5)

        i = i - 1
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False