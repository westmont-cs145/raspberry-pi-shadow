# -*- coding: utf-8 -*-
"""
Created on ???

@author: Claire Inglehart (pushed to Bitbucket repository by Joshua Cross)
modified by Joshua Cross
"""

from PIL import Image, ImageChops, ImageEnhance

"""
img1 is the first image, it is not affected by the offset
img2 is the second image, it is affected by the offset
xoff is the number of pixles img2 will be shifted to the right by (default = 0)
yoff is the number of pixles img2 will be shifted down by (default = 0)
"""
def imageCompilation(img1, img2, xoff=0, yoff=0):
    _img1 = img1
    _img2 = img2
    
    """
    _img1 = Image.open(img1, "r")
    _img2 = Image.open(img2, "r")
    """
    _img1 = _img1.convert("LA")
    _img2 = _img2.convert("LA")
    
    _img2 = ImageChops.offset(_img2, xoff, yoff)

    _diff = ImageChops.add(_img1, _img2)
    
    """
    uncomment out the next line to have it display the result on run everytime
    (for debugging purposes only)
    """
    #_diff.show()

    #the next line will need to be changed when we have a propper place to store images
    #_diff.save("alpha_add.png")   # saving the added image (having alpha channel)
    
    #_img1.close()
    #_img2.close()
    
    return(_diff)