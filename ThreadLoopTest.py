# -*- coding: utf-8 -*-
"""
Created on Sat Apr 17 14:15:02 2021

@author: Owner
"""

### Choose one option ###


###if we use busy waiting
#import _thread
###import time
#import CameraLoop
#
#def test():
#    p1 = CameraLoop()
#    try:
#        _thread.start_new_thread(p1.loop())
#    except:
#        print("thread loop error")
#
#    
#    p1.running=False
#    print(p1.getName())
#    p1.running=True
#
#test()
        
####################### or if we use thread object #################################
import threading
import CameraLoop
import time

class myThread (threading.Thread):
   def __init__(self, threadID, name):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
   def run(self):
	   p1.loop()
   
def getName():
	p1.running = False
	print(p1.getName())
	#thread1.exit()
	p1.running = True
	thread1.run()
	
def test():
	i=0
	while(i<10):
		time.wait(1)
		p1.running = False
		print(p1.mostRecentPic())
		#thread1.exit()
		p1.running = True
		thread1.run()
		i=i+1

# Create new thread
thread1 = myThread(1, "Thread-1")

# Start Camera object
p1 = CameraLoop()
#start thread
thread1.start()
