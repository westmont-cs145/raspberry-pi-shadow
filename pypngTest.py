# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 16:39:23 2021

@author: Andrew Day
"""

import png
from PIL import Image



def pypngTest(inputName, outputName, pixelBuffer):
    #get pixel 2d sizes
    f =open(inputName, 'rb')
    r=png.Reader(f)
    s = r.read()
    width = s[0] #s holds inforation of the picture being read
    height= s[1]
    f.close()

    #get pixels
    img=Image.open(inputName)
    pixels=list(img.getdata())
    img.close()
    
    #array of pixel color (shadow or not)
    new_pixels=[]

    #initiate array of searched pixels
        #get largest range of non green in row, add pixel positions (start,end) to array as new element  -- Should have row number of elements
    pixelRanges=[]
        

    count = 0
    for y in range(0,height):
        #array that saves all pixel (shadow or not) data per row
        row=[]
        
        #rowRanges - array that saves the different range elements per row seperately
        rowRanges=[]

        #tempRange - array that keeps track of each potential range and adds to rowRanges if larger than buffer
        tempRange=[-1, -1]
        tempStart = 0
        tempEnd = 0
        previousIsShadow = False #set it to black '0' first, white = '1'
        
        for x in range(0,width):
            #RGB values
            group = pixels[count]
            #get colors of given pixel
            red = group[0] 
            green = group[1]
            blue = group[2]
            
            if (green>red and green>blue): # if the pixel is more green than other make white(not shadow), otherwise make black(is shadow)
                if (previousIsShadow): #if previous was shadow, set that to the end of the range, check if new range is larger than sensitivity
                    tempEnd=x-1
                    if(tempEnd-tempStart > pixelBuffer):
                        #need to preserve range data and save them in a new array of size 2, appended to rowRanges
                        tempRange[0] = tempStart
                        tempRange[1] = tempEnd
                        rowRanges.append(tempRange)
                row.append(255) #white - not shadow
                row.append(0) #alpha 0 is translucent, 255 for opaque
                previousIsShadow = False
            else:
                if(not(previousIsShadow)): #if previous was not a Shadow, set current to tempStart
                    tempStart=x
                elif(previousIsShadow and x==width-1):
                    tempEnd = x
                    if(tempEnd-tempStart > pixelBuffer):
                        #need to preserve range data and save them in a new array of size 2, appended to rowRanges
                        tempRange[0] = tempStart
                        tempRange[1] = tempEnd
                        rowRanges.append(tempRange)
                row.append(0) #black - shadow
                row.append(255) #alpha, opaque
                previousIsShadow=True
            count = count+1
            
        #if the row has no shadows, then supply the array [-1,-1]
        if(len(rowRanges)==0):
            tempRange=[-1, -1]
            rowRanges.append(tempRange)
             
        pixelRanges.append(rowRanges)
            
        new_pixels.append(row) #the array to png function takes 2-d array, so add each orw as an element in the larger array
    

    png.from_array(new_pixels, 'LA').save(outputName) #LA - stands for greyscale alpha
    return(pixelRanges)


###       For testing specifc files at a time       ###
    
#pypngTest('test-rgb.png', 'test-out.png',1)