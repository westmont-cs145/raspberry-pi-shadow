# -*- coding: utf-8 -*-
"""
Created on Sat Apr 17 14:15:02 2021

@author: Owner
"""

import pygame
import time
import picamera
import sys

from pypngTest import pypngTest

pygame.init()



with picamera.PiCamera() as camera:

    camera.resolution = (1280, 720)
    camera.framerate = 30
    # Wait for the automatic gain control to settle
    time.sleep(2)
    # Now fix the values
    camera.shutter_speed = camera.exposure_speed
    camera.exposure_mode = 'off'
    g = camera.awb_gains
    camera.awb_mode = 'off'
    camera.awb_gains = g
    # Finally, take several photos with the fixed settings
    camera.capture_sequence(['im%02d.png' % i for i in range(5)])




im00 = pygame.image.load("im00.png")
im01 = pygame.image.load("im01.png")
im02 = pygame.image.load("im02.png")
im03= pygame.image.load("im03.png")
im04 = pygame.image.load("im04.png")


screen = pygame.display.set_mode(im00.get_size())
screen = pygame.display.set_mode(im01.get_size())
screen = pygame.display.set_mode(im02.get_size())
screen = pygame.display.set_mode(im03.get_size())
screen = pygame.display.set_mode(im04.get_size())

# a simple flag to show if the application is running
# there are other ways to do this, of course
running = True
while running:

    # it's important to get all events from the 
    # event queue; otherwise it may get stuck
    for e in pygame.event.get():
        # if there's a QUIT event (someone wants to close the window)
        # then set the running flag to False so the while loop ends
        if e.type == pygame.QUIT:
            running = False


    screen.blit(im00, (0,0))  
    pygame.display.flip()  
    time.sleep(0.5)

    screen.blit(im01, (0,0))  
    pygame.display.flip()  
    time.sleep(0.5)

    screen.blit(im02, (0,0)) 
    pygame.display.flip()  
    time.sleep(0.5)

    screen.blit(im03, (0,0))  
    pygame.display.flip()  
    time.sleep(0.5)

    screen.blit(im04, (0,0))  