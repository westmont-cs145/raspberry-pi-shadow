# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 12:58:49 2021

@author: Joshua Cross
"""

from PIL import ImageChops

class Obstacle:
    def __init__(self, baseImg, xoff, yoff):
        self._baseImg = baseImg
        self._xoff = xoff
        self._yoff = yoff
        self._baseImg = ImageChops.offset(self._baseImg, self._xoff, self._yoff)
    
    def getBaseImg(self):
        return self._baseImg
    
    def getXoff(self):
        return self._xoff
    
    def getYoff(self):
        return self._yoff
    
    def setBaseImg(self, baseImg):
        self._baseImg = baseImg
        
    def setXoff(self, xoff):
        self.xoff = xoff
        self._baseImg.offset(self.baseImg, self._xoff)
        
    def setYoff(self, yoff):
        self.yoff = yoff
        self._baseImg.offset(self.baseImg, 0, self._yoff)
    
    def pushDown(self, FALLPERUNIT):
        self._yoff = self._yoff + FALLPERUNIT
        self._baseImg = ImageChops.offset(self._baseImg, 0, FALLPERUNIT)
        