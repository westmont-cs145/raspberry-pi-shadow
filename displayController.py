# -*- coding: utf-8 -*-
"""
Created on Tue Apr 27 17:25:38 2021

@author: Andrew Day
"""
#import ThreadLoopTest
#from compareShadows import compareShadows
from pypngTest import pypngTest
from picamera import PiCamera
import time
import obstacleManager
#import imageCompilation
from compareShadows import compareShadows
from PIL import ImageChops, Image
import pygame
import sys
import threading


class myThread (threading.Thread):
   def __init__(self, threadID):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.camera = PiCamera()
      self.camera.resolution = (160, 144)
      self.camera.framerate = 30
      ##Wait for the automatic gain control to settle
      time.sleep(2)
      ## Now fix the values
      self.camera.shutter_speed = self.camera.exposure_speed
      self.camera.exposure_mode = 'off'
      self.g = self.camera.awb_gains
      self.camera.awb_mode = 'off'
      self.camera.awb_gains = self.g


   def run(self, picName):
      self.camera.capture('im'+str(picName)+'.png')


#TODO - add secondary argument that adjusts the size of the picture
def displayController(fallSpeed, sizeMultiple, sensitivity):
    #without loop
    picName=0

    pygame.init()
    running = True
    obstacles = obstacleManager.obstacleManager()
    print("Finished initialization commands. (Line 37)")

    screenSize = (160*sizeMultiple,144*sizeMultiple)

    screen = pygame.display.set_mode(screenSize)

    thread1 = myThread(1)
    thread1.start()

    #varTime= []

    ##############    start of loop ##################
    while(running):
        thread1.run(picName)
        #print("success, took a png")
        #create/update obstacles
        obstacleManager.obstacleManager(obstacles, fallSpeed)
        #create shadow and check for collision
        #print("finished obstacle update")
        rangeTable = []
        for i in range(0, len(obstacles)):
            if(i==0):
                fusion = obstacles[i].getBaseImg()
            else:
                fusion = ImageChops.add(fusion, obstacles[i].getBaseImg())
            rangeTable.append([obstacles[i].getXoff(),obstacles[i].getYoff()])
        #print("finished creating obstacles")

        shadowRange = pypngTest('im'+str(picName)+'.png', 'output.png', sensitivity)

        #pypngTest('im'+str(picName)+'.png', 'output.png', 4)
        #print("finish pypngTest")

        gameEnd = compareShadows(shadowRange, rangeTable)

        #print("finished pypngTest and Compareshadows")
        picName = (picName+1) % 10

        #print("Gets to beggining of display code block")

        #display the output.png and the fusion of obstacles


        #adding the fusion and back takes ~ 304405083
        shadow = Image.open('output.png')
        #shadow = shadow.convert("LA")

        fusion = fusion.convert("LA")

        #var = time.perf_counter_ns()

        fusion = ImageChops.add(fusion, shadow)
        shadow.close()

        fusion.save("testing1234.png")

        fusion = pygame.image.load("testing1234.png")
        back = pygame.image.load("blue.png")

        #screen.blit(pygame.transform.scale(back,screenSize), (0,0))
        #pygame.display.flip()
        screen.fill([0,0,144])

        screen.blit(pygame.transform.scale(fusion,screenSize), (0,0))
        pygame.display.flip()

        #var = time.perf_counter_ns()-var

        #varTime.append(var)

        #time.sleep(float(waitTime))

        #if(len(varTime)==20):
        #    sum = 0
        #    for i in range (0,20):
        #        sum=varTime[i]+sum
        #    sum=sum//20
        #    print(str(sum))
        ##print("Got to the end of the loop")

        #if a collision is detected, suspend the game, and transition into gameEnd sequence (#todo)
        if(gameEnd):
            gameOver = pygame.image.load("GameOver.png")
            screen.blit(pygame.transform.scale(gameOver,screenSize), (0,0))
            pygame.display.flip()
            print("Game Over")
            time.sleep(5)
            running = False
            #maybe cause some sort of function screen that waits for specific area collision (e.g) like a start new game box
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                print("exited the game")
                running = False
#testing
fallSpeed = int(sys.argv[1])
multiple = int(sys.argv[2])
sensitivity = int(sys.argv[3])
displayController(fallSpeed, multiple, sensitivity)