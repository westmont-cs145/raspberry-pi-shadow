import picamera
import pypngTest
import time
import sys

input = sys.argv[1]
output = sys.argv[2]

#inputFile = '/home/pi/shadowpy/'+input+'.png'
#outputFile = '/home/pi/shadowpy/'+output+'.png'

#now just keep images in same file as program
inputFile = input+'.png'
outputFile = output+'.png'


camera = picamera.PiCamera()
camera.resolution = (640, 480)
camera.start_preview()
time.sleep(1)
camera.capture(inputFile)
camera.stop_preview()
pypngTest(inputFile, outputFile)